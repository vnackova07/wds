<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Project;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function index()
    {

    }


    public function create()
    {
        return view("components.blog.create");
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'image_path' => 'required|image|mimes:png,jpg,jpeg,gif,svg|max:2048',
            'body' => 'required',
           ]);

           $input = $request->all();

           if($image = $request->file('image_path')){
            $destinationPath = 'img/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image_path'] = "$profileImage";
           }

           Blog::create($input);

           return redirect()->route('index')->with('success', 'Blog created successfully!');


    }


    public function show(Blog $blog)
    {
        return view('components.blog.show', compact('blog'));
    }


    public function edit(Blog $blog)
    {
        return view('components.blog.edit', compact('blog'));
    }


    public function update(Request $request, Blog $blog)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'body' => 'required',
        ]);

        $input = $request->all();

        if ($image = $request->file('image_path')){
            $destinationPath = 'img/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image_path'] = "$profileImage";
           }else{
            unset($input['image_path']);
           }

           $blog->update($input);

           return redirect()->route('index')->with('success', 'Blog updated successfully');
    }


    public function destroy(Blog $blog)
    {
        $blog->delete();

        return redirect()->route('index')->with('success', 'Blog deleted successfully');

    }
}
