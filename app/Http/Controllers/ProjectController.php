<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Project;
use Illuminate\Http\Request;




class ProjectController extends Controller
{

    public function index()
    {
        $projects = Project::latest()->paginate(5);
        $blogs = Blog::latest()->paginate(5);

        return view("components.projects.index", compact('projects', 'blogs'))->with('i', (request()->input('page',1)-1) * 5);
    }


    public function create()
    {
        return view("components.projects.create");
    }


    public function store(Request $request)
    {
       $request->validate([
        'name' => 'required',
        'detail' => 'required',
        'image' => 'required|image|mimes:png,jpg,jpeg,gif,svg|max:2048',
        'links' => 'required',
       ]);

       $input = $request->all();

       if($image = $request->file('image')){
        $destinationPath = 'img/';
        $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        $image->move($destinationPath, $profileImage);
        $input['image'] = "$profileImage";
       }

       Project::create($input);

       return redirect()->route('index')->with('success', 'Project created successfully!');


    }


    public function show(Project $project)
    {
        return view('components.projects.show', compact('project'));
    }


    public function edit(Project $project)
    {
        return view('components.projects.edit', compact('project'));
    }


    public function update(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'links' => 'required'
        ]);

        $input = $request->all();

        if ($image = $request->file('image')){
            $destinationPath = 'img/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
           }else{
            unset($input['image']);
           }

           $project->update($input);

           return redirect()->route('index')->with('success', 'Product updated successfully');
        }



    public function destroy(Project $project)
    {
        $project->delete();

        return redirect()->route('index')->with('success', 'Product deleted successfully');

    }
}
