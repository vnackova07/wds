<?php

namespace App\Http\Controllers;
use App\Models\Blog;
use App\Models\Project;
use Illuminate\Http\Request;
use Faker\Provider\ar_EG\Company;

class HomeController extends Controller
{

    public function index()
    {
        $projects = Project::all();
        $blogs = Blog::all();
        return view("home", compact('projects', 'blogs'));
    }


    public function show($id)
    {
        //
    }
}
