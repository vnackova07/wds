<x-app-layout>

<!-- ====== Table Section Start -->
<section class="bg-white py-10 lg:py-[50px]">
    <div class="container">
        <div class="flex justify-between">
        <h2 class="text-4xl">WDS Admin Panel</h2>
        <a href="{{ route('projects.create') }}"><button type="button" class="text-white bg-blue hover:bg-gray-900 focus:outline-none focus:ring-4 focus:ring-gray-300 font-bold rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700">Create New Project</button></a>
    </div>
    @if($message = Session::get('success'))
        <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800 py-8" role="alert">
            <span class="font-bold"><p> {{ $message }}</p>
          </div>
          @endif

        <div class="flex flex-wrap -mx-4 mt-12">
            <div class="w-full px-4">
                <div class="max-w-full overflow-x-auto">
                    <table class="table-auto w-full">
                        <thead>
                            <tr class="bg-blue text-center">
                                <th class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    No
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">Image
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Name
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Details
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Project Url
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            @if (!empty($projects))
                                @foreach ($projects as $project)
                                    <td class="text-center text-dark-gray font-medium text-base py-5 px-2 bg-[#F3F6FF] border-b border-1 border-[#E8E8E8] text-bold">{{ ++$i }}</td>
                                    <td class="text-center text-dark font-medium text-base py-5 px-2 bg-white border-b border-[#E8E8E8]"><img src="/img/{{ $project->image }}" width="200px"></td>
                                    <td class="text-center text-dark-gray font-medium text-base py-5 px-2 bg-[#F3F6FF] border-b border-1 border-[#E8E8E8] text-bold"> {{ $project->name }}</td>
                                    <td class="text-center text-dark font-medium text-base py-5 px-2 bg-white border-b border-[#E8E8E8] text-bold"> {{  Str::words($project->detail, 10)  }}</td>
                                    <td class="text-center text-dark font-medium text-base py-5 px-2 bg-[#F3F6FF] border-b border-[#E8E8E8] text-bold"> {{ $project->links }}</td>
                                    <td class="text-center text-dark-gray font-medium text-base py-5 px-2 bg-white border-b border-r border-1 border-[#E8E8E8]">

                                    <a href="{{ route('projects.show', $project->id)}}" class="text-white font-bold bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300  rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Show</a>
                                    <a href="{{ route('projects.edit', $project->id)}}" class="text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-4 focus:ring-yellow-300 font-bold rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:focus:ring-yellow-900">Edit</a>

                                    <form action="{{ route('projects.destroy', $project->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class=" mt-4 text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-bold rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Delete</button>

                                    </form>
                                    </td>
                                    </tr>
                                    @endforeach
                                @endif


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {!! $projects->links() !!}
</section>

<section class="bg-white py-10 lg:py-[50px]">
    <div class="container">
        <div class="flex justify-between">
            <h2 class="text-4xl">Blog Posts</h2>
            <a href="{{ route('blog.create') }}"><button type="button"
                    class="text-white bg-blue hover:bg-gray-900 focus:outline-none focus:ring-4 focus:ring-gray-300 font-bold rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700">Create
                    New Blog</button></a>
        </div>

        <div class="flex flex-wrap -mx-4 mt-12">
            <div class="w-full px-4">
                <div class="max-w-full overflow-x-auto">
                    <table class="table-auto w-full">
                        <thead>
                            <tr class="bg-blue text-center">
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    No
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Image
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Title
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Slug
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Description
                                </th>
                                <th
                                    class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                    Created at
                                </th>
                                <th
                                class="w-1/6 min-w-[160px] text-lg font-bold text-white py-4 lg:py-7 px-3 lg:px-4 border-l border-white">
                                Action
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @if (!empty($blogs))
                                    @foreach ($blogs as $blog)
                                        <td
                                            class="text-center text-dark-gray bg-[#F3F6FF] border-b border-1 border-[#E8E8E8] text-bold">
                                            {{ ++$i }}</td>
                                        <td
                                            class="text-center text-dark font-medium text-base py-5 px-2 bg-white border-b border-[#E8E8E8]">
                                            <img src="/img/{{ $blog->image_path }}" width="200px"></td>

                                        <td
                                            class="text-center text-dark-gray font-medium text-base py-5 px-2 bg-[#F3F6FF] border-b border-1 border-[#E8E8E8] text-bold">
                                            {{ $blog->title }}</td>
                                        <td
                                            class="text-center text-dark font-medium text-base py-5 px-2 bg-white border-b border-[#E8E8E8] text-bold">
                                            {{ $blog->slug }}</td>
                                        <td
                                            class="text-center text-dark font-medium text-base py-5 px-2 bg-[#F3F6FF] border-b border-[#E8E8E8] text-bold text-ellipsis overflow-hidden">
                                            {{ Str::words($blog->body, 2) }}</td>
                                            <td
                                            class="text-center text-dark font-medium text-base py-5 px-2 bg-white border-b border-[#E8E8E8] text-bold">
                                            {{ $blog->created_at }}</td>
                                        <td
                                            class="text-center text-dark-gray font-medium text-base py-5 bg-[#F3F6FF] border-b border-1 border-[#E8E8E8]">

                                            <a href="{{ route('blog.show', $blog->id) }}"
                                                class="text-white font-bold bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300  rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Show</a>
                                            <a href="{{ route('blog.edit', $blog->id) }}"
                                                class="text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-4 focus:ring-yellow-300 font-bold rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:focus:ring-yellow-900">Edit</a>

                                            <form action="{{ route('blog.destroy', $blog->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class=" mt-4 text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 font-bold rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">Delete</button>

                                            </form>
                                        </td>
                            </tr>
                            @endforeach
                            @endif


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {!! $blogs->links() !!}
</section>
</x-app-layout>
<!-- ====== Table Section End -->
