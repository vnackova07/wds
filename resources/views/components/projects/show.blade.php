<x-app-layout>
    <section class="bg-white py-8 lg:py-[25px] h-screen">
        <div class="container">
            <div class="flex justify-between mb-2">
            <h2 class="text-4xl mb-4 font-bold">Show Project</h2>
            <a href="{{ url('dashboard') }}"><button type="button" class="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-bold rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Back</button>
            </button></a>
            </div>
            <div class="w-3/4 flex flex-col">
                <div class="form-group flex text-2xl">
                    <h3 class="h3 font-bold pr-2">Name: </h3><span> {{ $project->name }}</span></div>
            </div>
            <div class="form-group flex text-2xl mt-1">
                <h3 class="h3 font-bold pr-2">Details: </h3><span> {{ $project->detail }}</span>
            </div>
            <div class="form-group flex text-2xl mt-1">
                <h3 class="h3 font-bold pr-2">Project Url: </h3><span> {{ $project->links }}</span>
            </div>

            <img src="/img//{{ $project->image }}" class="max-w-full h-auto mt-8" alt="..." />


        </div>
    </section>
</x-app-layout>
