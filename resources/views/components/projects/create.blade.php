<x-app-layout>
    @section('head')
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
    @endsection
    <section class="bg-white py-20 lg:py-[50px] h-screen">
        <div class="container">
            <h2 class="text-4xl mb-4 font-bold">Create New Project</h2>
            <a href="{{ url('dashboard') }}"><button type="button" class="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-bold rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Back</button>
            </button>
            </a>

            @if($errors->any())
            <div class="font-bold p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                <span class="font-bold">Whoops! </span>There were some problems with your input.
            </div>
              <ul class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert"">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif


            <form action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="flex flex-wrap flex-col -mx-4 mt-8">
                <div class="w-full md:w-1/2 lg:w-full px-4">
                   <div class="mb-6">
                    <div class="form-group">
                      <label for="" class="font-bold text-base text-black block mb-3">
                      Project Name
                      </label>
                      <input type="text" name="name" placeholder="Project Name" class="
                         form-control
                         w-full
                         border-[1.5px] border-form-stroke
                         rounded-lg
                         py-3
                         px-5
                         font-medium
                         text-body-color
                         placeholder-body-color
                         outline-none
                         focus:border-primary
                         active:border-primary
                         transition
                         disabled:bg-[#F5F7FD] disabled:cursor-default
                         ">
                   </div>
                   </div>
                </div>
                <div class="w-full md:w-1/2 lg:w-full px-4">
                    <div class="mb-6">
                    <div class="form-group">
                       <label for="" class="font-bold text-base text-black block mb-3">
                       Project Details
                       </label>
                       <textarea id="editor" rows="5" cols="16" placeholder="Project Details" name="detail" class="
                          form-control
                          w-full
                          border-[1.5px] border-form-stroke
                          rounded-lg
                          py-3
                          px-5
                          font-medium
                          text-body-color
                          placeholder-body-color
                          outline-none
                          focus:border-primary
                          active:border-primary
                          transition
                          disabled:bg-[#F5F7FD] disabled:cursor-default
                          "></textarea>
                    </div>
                    </div>
                 </div>
                 <div class="w-full md:w-1/2 lg:w-full px-4">
                    <div class="mb-6">
                     <div class="form-group">
                       <label for="" class="font-bold text-base text-black block mb-3">
                       Url
                       </label>
                       <input type="text" name="links" placeholder="Url" class="
                          form-control
                          w-full
                          border-[1.5px] border-form-stroke
                          rounded-lg
                          py-3
                          px-5
                          font-medium
                          text-body-color
                          placeholder-body-color
                          outline-none
                          focus:border-primary
                          active:border-primary
                          transition
                          disabled:bg-[#F5F7FD] disabled:cursor-default
                          ">
                    </div>
                    </div>
                 </div>

                    <div class="w-full lg:w-1/2 px-4">
                    <div class="form-group">
                       <div class="mb-6">
                          <label for="" class="font-bold text-base text-black block mb-3">
                          Image
                          </label>
                          <input type="file" placeholder="Image" name="image" class="
                             form-control
                             w-full
                             border-[1.5px] border-form-stroke
                             rounded-lg
                             font-medium
                             text-body-color
                             placeholder-body-color
                             outline-none
                             focus:border-primary
                             active:border-primary
                             transition
                             disabled:bg-[#F5F7FD] disabled:cursor-default
                             cursor-pointer
                             file:bg-[#F5F7FD]
                             file:border-0
                             file:border-solid
                             file:border-r
                             file:border-collapse
                             file:border-form-stroke
                             file:py-3
                             file:px-5
                             file:mr-5
                             file:text-body-color
                             file:cursor-pointer
                             file:hover:bg-primary
                             file:hover:bg-opacity-10
                             ">
                       </div>
                    </div>
                    </div>


                 </div>
                 <button type="submit" class="text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800 font-bold">Submit</button>
            </form>
        </div>
    </section>

    @section('scripts')
    <script>
        ClassicEditor
                .create( document.querySelector( '#editor' ) )
                .then( editor => {
                        console.log( editor );
                } )
                .catch( error => {
                        console.error( error );
                } );
    </script>
    @endsection


</x-app-layout>
