<div {{ $attributes->class(['flex items-center mb-0'])}}>
    <a
       href="https://gitlab.com/vnackova07"
       target="_blank"
       class="
       w-10
       h-10
       flex
       items-center
       justify-center
       rounded-full
       border-2
     border-dark
     dark:border-white
     dark:text-white
     text-dark
       hover:text-dark hover:bg-orange-500 hover:border-white
       mr-3
       sm:mr-4
       lg:mr-3
       xl:mr-4
       transition duration-300 delay-150 hover:delay-0 hover:ease-in-out"
       >
       <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-gitlab"><path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"></path></svg>
    </a>

    <a
       href="https://github.com/nackovav07"
       target="_blank"
       class="
       w-10
       h-10
       flex
       items-center
       justify-center
       rounded-full
       border-2
     border-white
     dark:text-white
       dark:hover:text-dark hover:bg-white dark:hover:border-purple
       mr-3
       sm:mr-4
       lg:mr-3
       xl:mr-4
       transition duration-300 delay-150 hover:delay-0 hover:ease-in-out"
       >
       <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-github" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"> <path stroke="none" d="M0 0h24v24H0z" fill="none"/> <path d="M9 19c-4.3 1.4 -4.3 -2.5 -6 -3m12 5v-3.5c0 -1 .1 -1.4 -.5 -2c2.8 -.3 5.5 -1.4 5.5 -6a4.6 4.6 0 0 0 -1.3 -3.2a4.2 4.2 0 0 0 -.1 -3.2s-1.1 -.3 -3.5 1.3a12.3 12.3 0 0 0 -6.2 0c-2.4 -1.6 -3.5 -1.3 -3.5 -1.3a4.2 4.2 0 0 0 -.1 3.2a4.6 4.6 0 0 0 -1.3 3.2c0 4.6 2.7 5.7 5.5 6c-.6 .6 -.6 1.2 -.5 2v3.5" /> </svg>
    </a>
    <a
       href="https://www.linkedin.com/in/nackovav/"
       target="_blank"
       class="
       w-10
       h-10
       flex
       items-center
       justify-center
       rounded-full
       border-2
     border-dark
     dark:border-white
     dark:text-white
     text-dark
     hover:text-dark hover:bg-primary hover:border-primary
       mr-3
       sm:mr-4
       lg:mr-3
       xl:mr-4
       transition duration-300 delay-150 hover:delay-0 hover:ease-in-out"
       >
       <svg
          width="18"
          height="18"
          viewBox="0 0 14 14"
          class="fill-current"
          >
          <path
             d="M13.0214 0H1.02084C0.453707 0 0 0.451613 0 1.01613V12.9839C0 13.5258 0.453707 14 1.02084 14H12.976C13.5432 14 13.9969 13.5484 13.9969 12.9839V0.993548C14.0422 0.451613 13.5885 0 13.0214 0ZM4.15142 11.9H2.08705V5.23871H4.15142V11.9ZM3.10789 4.3129C2.42733 4.3129 1.90557 3.77097 1.90557 3.11613C1.90557 2.46129 2.45002 1.91935 3.10789 1.91935C3.76577 1.91935 4.31022 2.46129 4.31022 3.11613C4.31022 3.77097 3.81114 4.3129 3.10789 4.3129ZM11.9779 11.9H9.9135V8.67097C9.9135 7.90323 9.89082 6.8871 8.82461 6.8871C7.73571 6.8871 7.57691 7.74516 7.57691 8.60323V11.9H5.51254V5.23871H7.53154V6.16452H7.55423C7.84914 5.62258 8.50701 5.08065 9.52785 5.08065C11.6376 5.08065 12.0232 6.43548 12.0232 8.2871V11.9H11.9779Z"
             />
       </svg>
    </a>
 </div>
