<x-app-layout>
    @section('head')
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
    @endsection
    <section class="bg-white py-20 lg:py-[5px] w-full h-screen">
        <div class="container">
            <div class="flex justify-between mt-1">
            <h2 class="text-4xl mb-4 font-bold">Edit Blog Post</h2>
            <a href="{{ url('dashboard') }}"><button type="button" class="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-bold rounded-full text-sm px-5 py-2.5 text-center  mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Back</button>
            </button>
            </a>
            </div>
            @if($errors->any())
            <div class="font-bold p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                <span class="font-bold">Whoops! </span>There were some problems with your input.
            </div>
              <ul class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert"">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif


            <form action="{{ route('blog.update', $blog->id) }}" method="POST" enctype="multipart/form-data">
            @csrf

            @method('PUT')

            <div class="flex flex-wrap flex-col -mx-4 ">
                <div class="w-full md:w-f lg:w-full px-4">
                   <div class="mb-4">
                    <div class="form-group">
                      <label for="" class="font-bold text-base text-red-600 block mb-3">
                      Blog Title
                      </label>
                      <input type="text" value="{{ $blog->title }}" name="title" placeholder="Blog Title" class="
                         form-control
                         w-full
                         border-[1.5px] border-form-stroke
                         rounded-lg
                         py-3
                         px-5
                         font-medium
                         text-body-color
                         placeholder-body-color
                         outline-none
                         focus:border-primary
                         active:border-primary
                         transition
                         disabled:bg-[#F5F7FD] disabled:cursor-default
                         ">
                   </div>
                   </div>
                </div>
                <div class="w-full md:w-1/2 lg:w-full px-4">
                    <div class="mb-4">
                     <div class="form-group">
                       <label for="" class="font-bold text-base text-red-600 block mb-3">
                       Blog Slug
                       </label>
                       <input type="text" value="{{ $blog->slug }}" name="slug" placeholder="Blog Slug" class="
                          form-control
                          w-full
                          border-[1.5px] border-form-stroke
                          rounded-lg
                          py-3
                          px-5
                          font-medium
                          text-body-color
                          placeholder-body-color
                          outline-none
                          focus:border-primary
                          active:border-primary
                          transition
                          disabled:bg-[#F5F7FD] disabled:cursor-default
                          ">
                    </div>
                    </div>
                 </div>
                <div class="w-full md:w-1/2 lg:w-full px-4">
                    <div class="mb-2">
                    <div class="form-group">
                       <label for="" class="font-bold text-base text-red-600 block mb-3">
                       Blog Description
                       </label>
                       <textarea id="editor" rows="5" cols="16" name="body" class="
                          form-control
                          w-full
                          border-[1.5px] border-form-stroke
                          rounded-lg
                          py-3
                          px-5
                          font-medium
                          text-body-color
                          placeholder-body-color
                          outline-none
                          focus:border-primary
                          active:border-primary
                          transition
                          disabled:bg-[#F5F7FD] disabled:cursor-default
                          ">{{ $blog->body }}</textarea>
                    </div>
                    </div>
                 </div>

                    <div class="w-full lg:w-1/2 px-4">
                    <div class="form-group">
                       <div class="mb-2">
                          <label for="" class="font-bold text-base text-red-600 block mb-3">
                          Image
                          </label>
                          <input type="file" placeholder="Image" name="image_path" class="
                             form-control
                             w-full
                             border-[1.5px] border-form-stroke
                             rounded-lg
                             font-medium
                             text-body-color
                             placeholder-body-color
                             outline-none
                             focus:border-primary
                             active:border-primary
                             transition
                             disabled:bg-[#F5F7FD] disabled:cursor-default
                             cursor-pointer
                             file:bg-[#F5F7FD]
                             file:border-0
                             file:border-solid
                             file:border-r
                             file:border-collapse
                             file:border-form-stroke
                             file:py-3
                             file:px-5
                             file:mr-5
                             file:text-body-color
                             file:cursor-pointer
                             file:hover:bg-primary
                             file:hover:bg-opacity-10
                             ">
                             <img src="/img/{{ $blog->image_path}}" alt="" width="700px" height="400px" class="mt-2">
                       </div>
                    </div>
                    </div>

                 </div>
                 <button type="submit" class="text-white bg-green-700 hover:bg-green-800 focus:outline-none focus:ring-4 focus:ring-green-300 rounded-full text-sm px-5 py-2 text-center mr-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800 font-bold">Submit</button>
            </form>
        </div>
    </section>

    @section('scripts')
    <script>
        ClassicEditor
                .create( document.querySelector( '#editor' ) )
                .then( editor => {
                        console.log( editor );
                } )
                .catch( error => {
                        console.error( error );
                } );
    </script>
    @endsection

</x-app-layout>
