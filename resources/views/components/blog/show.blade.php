<x-app-layout>
    <section class="bg-white py-8 lg:py-[55px] h-screen w-full">
        <div class="container">
            <div class="flex justify-between">
            <h2 class="text-4xl mb-4 font-bold">Show Project</h2>
            <a href="{{ url('dashboard') }}"><button type="button" class="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-bold rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Back</button>
            </button></a>
            </div>
            <div class="w-full flex flex-col">
                <div class="form-group flex text-2xl text-red-600 font-bold ">
                    <h3 class="h3 pr-2">Title: </h3><span class="text-black"> {{ $blog->title }}</span></div>
            </div>
            <div class="form-group flex text-2xl text-red-600 font-bold">
                <h3 class="h3 font-bold pr-2">Slug: </h3><span class="text-black"> {{ $blog->slug }}</span>
            </div>
            <div class="form-group flex text-2xl text-red-600">
                <h3 class="h3 font-bold pr-2">Details: </h3><span class="text-black"> {{ $blog->body }}</span>
            </div>
            <div class="form-group flex text-2xl text-red-600">
                <h3 class="h3 font-bold pr-2">Created At: </h3><span class="text-black"> {{ $blog->created_at }}</span>
            </div>

            <img src="/img//{{ $blog->image_path }}" class="w-8/12 h-1/4" alt="..." />


        </div>
    </section>
</x-app-layout>
