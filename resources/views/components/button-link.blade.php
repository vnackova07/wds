<a
href="{{$href}}"
target="{{$target}}"
{{
$attributes->class([
    'py-2 lg:py-4 px-6 md:px-9 lg:px-6 xl:px-9 rounded leading-normal border inline-block transition',
    'hover:bg-primary hover:border-primary hover:text-white' => $variant = 'outline-primary',
    'hover:bg-red-700 hover:border-red-700 hover:text-white' => $variant = 'outline-red',
    'bg-red-700 border-red-700 hover:text-white hover:bg-red-800' => $variant = 'red',
    'bg-gray-900 border-gray-700 text-white hover:bg-purple' => $variant = 'dark',
    'bg-purple border-purple text-white hover:bg-white hover:text-dark hover:border-dark' => $variant = 'primary',
    'bg-blue border-blue text-white hover:bg-white hover:text-dark hover:border-dark' => $variant = 'blue',
])
}}>

{{ $slot }}

</a>
