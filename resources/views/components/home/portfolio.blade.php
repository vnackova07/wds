<!-- ====== Portfolio Section Start -->
{{-- <section id="portfolio" class="dark:bg-dark-gray bg-white pt-20 lg:pt-[120px] pb-12 lg:pb-[90px]  border-b-[1px] border-white border-opacity-20">
    <div class="container">
        <div class="flex flex-wrap -mx-4">
                <div class="w-full px-4">
                    <div class="text-center mx-auto mb-12 lg:mb-20 max-w-[510px]">
                        <h2 class="font-bold text-3xl sm:text-4xl md:text-[40px] text-purple mb-4">
                            Our Services
                        </h2>
                    </div>
                </div>
                @foreach ($projects as $project)
                <div class="w-full md:w-1/2 xl:w-1/3 px-4">
                <div class="service-icon rounded-lg overflow-hidden mb-10">
                    <img
                       src="/img/{{ $project->image }}"
                       alt="image"
                       class="w-full"
                       />
                    <div class="p-8 sm:p-9 md:p-7 xl:p-9 text-center">
                       <h3>
                          <a
                             href="javascript:void(0)"
                             class="font-semibold text-white text-xl sm:text-[22px] md:text-xl lg:text-[22px] xl:text-xl 2xl:text-[22px] mb-4 block hover:text-purple" >
                               {{$project->name}}
                          </a>
                       </h3>
                       <p class="text-base text-body-color leading-relaxed mb-7">
                         {{ $project->detail}}
                       </p>

                    </div>
                 </div>
                </div>
                @endforeach
              </div>
            </div>
</section> --}}

<section id="portfolio" class="dark:bg-dark-gray bg-white pt-20 lg:pt-[120px] pb-12 lg:pb-[150px]  border-b-[1px] border-white border-opacity-20">

    <style>
      .zoom:hover img {
        transform: scale(1.1);
      }
    </style>
<div class="container">
    <div class="w-full px-4">
        <div class="text-center mx-auto mb-12 lg:mb-20 max-w-[510px]">
            <h1 class="ml10 font-bold text-4xl sm:text-4xl md:text-[36px] text-turquoise mb-4">
                <span class="text-wrapper">
                  <span class="letters">Our Projects</span>
                </span>
              </h1>
            <p class="text-base text-white">
                There are many variations of passages of Lorem Ipsum available
                but the majority have suffered alteration in some form.
            </p>
        </div>
    </div>

    <div class="grid lg:grid-cols-3 gap-6 pb-20">
        @foreach ($projects as $project)
      <div class="zoom shadow-lg rounded-lg relative overflow-hidden bg-no-repeat bg-cover bg-center"
        style="background-position: 50%;" data-mdb-ripple="true" data-mdb-ripple-color="dark">
        <img src="/img/{{ $project->image }}"
          class="w-full transition duration-300 ease-linear align-middle h-64" />
        <a href="#!">

          <div class="hover-overlay">
            <div
              class="mask absolute top-0 right-0 bottom-0 left-0 w-full h-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 py-20"
              style="background-color: rgba(10, 3, 3, 0.699)">
              <div class="flex justify-center items-center pt-2 flex-col"><a href="" class="text-dark rounded-full py-7 px-6 bg-turquoise text-center font-thin text-[12px] tracking-widest">View <div>Project</div></a>
              </div>
            </div>
          </div>
        </a>
      </div>
      @endforeach
    </div>
</div>

</section>
