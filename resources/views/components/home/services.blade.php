<!-- ====== Services Section Start -->
<section id="services" class="dark:bg-dark-gray bg-white pt-20 lg:pt-[120px] pb-12 lg:pb-[90px]  border-b-[1px] border-white border-opacity-20">
    <div class="container">
        <div class="flex flex-wrap -mx-4">
            <div class="w-full px-4">
                <div class="text-center mx-auto mb-12 lg:mb-20 max-w-[510px]">
                    <h1 class="ml11 font-bold text-4xl sm:text-4xl md:text-[36px] text-turquoise mb-4">
                        <span class="text-wrapper">
                          <span class="line line1"></span>
                          <span class="letters">Our Services</span>
                        </span>
                      </h1>
                </div>
            </div>
        </div>
        <div  class="flex flex-wrap -mx-4 h-full">
            <div data-tilt class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div class="service-icon p-10 md:px-7 xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-device-mobile" width="34px" height="34px" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <rect x="6" y="3" width="12" height="18" rx="2" fill="none"></rect>
                    <line x1="11" y1="4" x2="13" y2="4" fill="white"></line>
                    <line x1="12" y1="17" x2="12" y2="17.01" fill="white"></line>
                 </svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        Front-End Development
                    </h4>
                    <p class="text-body-color">
                        We love working on complex web applications and delivering easy to use, beautiful products.
                    </p>
                </div>
            </div>
            <div data-tilt class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div
                    class="service-icon
                p-10
                md:px-7
                xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg width="32px" height="32px" viewBox="0 -64 640 640" xmlns="http://www.w3.org/2000/svg"><path d="M255.03 261.65c6.25 6.25 16.38 6.25 22.63 0l11.31-11.31c6.25-6.25 6.25-16.38 0-22.63L253.25 192l35.71-35.72c6.25-6.25 6.25-16.38 0-22.63l-11.31-11.31c-6.25-6.25-16.38-6.25-22.63 0l-58.34 58.34c-6.25 6.25-6.25 16.38 0 22.63l58.35 58.34zm96.01-11.3l11.31 11.31c6.25 6.25 16.38 6.25 22.63 0l58.34-58.34c6.25-6.25 6.25-16.38 0-22.63l-58.34-58.34c-6.25-6.25-16.38-6.25-22.63 0l-11.31 11.31c-6.25 6.25-6.25 16.38 0 22.63L386.75 192l-35.71 35.72c-6.25 6.25-6.25 16.38 0 22.63zM624 416H381.54c-.74 19.81-14.71 32-32.74 32H288c-18.69 0-33.02-17.47-32.77-32H16c-8.8 0-16 7.2-16 16v16c0 35.2 28.8 64 64 64h512c35.2 0 64-28.8 64-64v-16c0-8.8-7.2-16-16-16zM576 48c0-26.4-21.6-48-48-48H112C85.6 0 64 21.6 64 48v336h512V48zm-64 272H128V64h384v256z" fill="black"/></svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        Back-End Development
                    </h4>
                    <p class="text-body-color">
                        Creating custom applications and providing unique Laravel-based solutions.
                    </p>
                </div>
            </div>
            <div data-tilt class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div
                    class="service-icon
                p-10
                md:px-7
                xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg width="32px" height="32px" viewBox="0 0 24 24" id="magicoon-Filled" xmlns="http://www.w3.org/2000/svg" fill="black"><defs><style>.cls-1{fill:black;}</style></defs><title>users-three</title><g id="users-three-Filled"><path id="users-three-Filled-2" data-name="users-three-Filled" d="M20.35,6.71a3.694,3.694,0,0,1-2.61,3.53A5.91,5.91,0,0,0,18,8.5a5.99,5.99,0,0,0-2.93-5.15A3.769,3.769,0,0,1,16.65,3,3.707,3.707,0,0,1,20.35,6.71ZM19.6,11.3a1.646,1.646,0,0,0-1.25.15,3.33,3.33,0,0,1-1.39.41,5.1,5.1,0,0,1-.7.85A5.425,5.425,0,0,1,19.92,17h.69a1.228,1.228,0,0,0,1.09-.63,2.551,2.551,0,0,0,.3-1.21v-.71A3.241,3.241,0,0,0,19.6,11.3ZM8.93,3.35A3.769,3.769,0,0,0,7.35,3a3.7,3.7,0,0,0-1.09,7.24A5.91,5.91,0,0,1,6,8.5,5.99,5.99,0,0,1,8.93,3.35ZM7.74,12.71a5.1,5.1,0,0,1-.7-.85,3.33,3.33,0,0,1-1.39-.41A1.646,1.646,0,0,0,4.4,11.3,3.241,3.241,0,0,0,2,14.45v.71a2.551,2.551,0,0,0,.3,1.21A1.224,1.224,0,0,0,3.38,17h.7A5.425,5.425,0,0,1,7.74,12.71Zm7.84,1.36a2.333,2.333,0,0,0-.53-.07,2.033,2.033,0,0,0-.99.26,4.268,4.268,0,0,1-4.12,0A2.033,2.033,0,0,0,8.95,14a2.333,2.333,0,0,0-.53.07A3.955,3.955,0,0,0,5.5,17.9v.87a3.073,3.073,0,0,0,.37,1.46A1.455,1.455,0,0,0,7.18,21h9.64a1.455,1.455,0,0,0,1.31-.77,3.073,3.073,0,0,0,.37-1.46V17.9A3.955,3.955,0,0,0,15.58,14.07ZM16.5,8.5A4.5,4.5,0,1,0,12,13,4.5,4.5,0,0,0,16.5,8.5Z" fill="black"/></g></svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        UI / UX Design
                    </h4>
                    <p class="text-body-color">
                        Websites not only need to look good, but they also need to be effective.
                    </p>
                </div>
            </div>
            <div data-tilt class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div
                    class="service-icon
                p-10
                md:px-7
                xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg width="32px" height="32px" viewBox="0 0 24 24" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style>.cls-1{fill:white;}.cls-2{clip-path:url(#clip-path);}</style><clipPath id="clip-path"><rect class="cls-1" width="32" height="32"/></clipPath></defs><title>marketing</title><g class="cls-2"><path d="M21.33,18.13l-.2,0L2.48,14.38a1,1,0,0,1-.81-1V9.67a1,1,0,0,1,.81-1L21.13,5a1,1,0,0,1,.83.2,1,1,0,0,1,.37.78V17.13a1,1,0,0,1-.37.77A1,1,0,0,1,21.33,18.13ZM3.67,12.58l16.66,3.33V7.16L3.67,10.49Z" fill="black"/><path d="M2.67,16.26a1,1,0,0,1-1-1V7.8a1,1,0,1,1,2,0v7.46A1,1,0,0,1,2.67,16.26Z" fill="black"/><path d="M9.67,19.06A4.27,4.27,0,0,1,5.4,14.8a3.94,3.94,0,0,1,.09-.87,1,1,0,0,1,1.2-.75,1,1,0,0,1,.75,1.2,2.39,2.39,0,0,0,0,.42,2.27,2.27,0,0,0,4.49.45,1,1,0,0,1,1.17-.79,1,1,0,0,1,.79,1.18A4.28,4.28,0,0,1,9.67,19.06Z" fill="black"/><path d="M17.52,17.2a1,1,0,0,1-1-1V6.7a1,1,0,1,1,2,0v9.5A1,1,0,0,1,17.52,17.2Z" fill="black"/></g></svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        Marketing Materials
                    </h4>
                    <p class="text-body-color">
                        Content marketing strategies that go beyond your expectations.
                    </p>
                </div>
            </div>
            <div data-tilt class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div
                    class="service-icon
                p-10
                md:px-7
                xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg width="28px" height="28px" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M167.02 309.34c-40.12 2.58-76.53 17.86-97.19 72.3-2.35 6.21-8 9.98-14.59 9.98-11.11 0-45.46-27.67-55.25-34.35C0 439.62 37.93 512 128 512c75.86 0 128-43.77 128-120.19 0-3.11-.65-6.08-.97-9.13l-88.01-73.34zM457.89 0c-15.16 0-29.37 6.71-40.21 16.45C213.27 199.05 192 203.34 192 257.09c0 13.7 3.25 26.76 8.73 38.7l63.82 53.18c7.21 1.8 14.64 3.03 22.39 3.03 62.11 0 98.11-45.47 211.16-256.46 7.38-14.35 13.9-29.85 13.9-45.99C512 20.64 486 0 457.89 0z" fill="black"/></svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        Branding
                    </h4>
                    <p class="text-body-color">
                        Branding has never been more expansive, adventurous and agile than it is today.
                    </p>
                </div>
            </div>
            <div  class="w-full md:w-1/2 lg:w-1/3 px-4">
                <div data-tilt
                    class="service-icon
                p-10
                md:px-7
                xl:px-10
                rounded-[20px]
                shadow-md
                hover:shadow-lg
                mb-8
                ">
                    <div
                        class="
                   w-[70px]
                   h-[70px]
                   flex
                   items-center
                   justify-center
                   bg-turquoise
                   rounded-2xl
                   mb-8
                   ">
                   <svg width="32px" height="32px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m21.512 6.112-3.89 3.889-3.535-3.536 3.889-3.889a6.501 6.501 0 0 0-8.484 8.486l-6.276 6.275a.999.999 0 0 0 0 1.414l2.122 2.122a.999.999 0 0 0 1.414 0l6.275-6.276a6.501 6.501 0 0 0 7.071-1.414 6.504 6.504 0 0 0 1.414-7.071z" fill="black"/></svg>
                    </div>
                    <h4 class="font-semibold text-xl text-dark dark:text-white mb-3">
                        Website Maintenance
                    </h4>
                    <p class="text-body-color">
                        We can help your website keep up with the ever-changing and evolving industry.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== Services Section End -->
