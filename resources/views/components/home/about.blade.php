<section id="about" class="dark:bg-dark-gray bg-white pt-20 lg:pt-[120px] pb-12 lg:pb-[90px] overflow-hidden border-b-[1px] border-white border-opacity-20 h-screen flex ">
    <div class="flex flex-wrap items-center -mx-4">
        <div class="w-full lg:w-6/12 px-4 flex justify-center h-fit lg:h-full">
            <div class="flex justify-center -mx-3 sm:-mx-4">
                <div id="tag"></div>
            </div>
        </div>
        <div class="w-full lg:w-1/2 xl:w-5/12 px-4">
            <div class="mt-10 lg:mt-0">
                <h2 class="font-bold text-3xl sm:text-4xl dark:text-purple mb-8">
                    Why Choose Us?
                </h2>
                <p class="text-base dark:text-gray-400 mb-8">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate repellendus doloribus praesentium inventore reiciendis dolorem ad nobis soluta provident exercitationem error, tempore quia iure eum tempora excepturi veniam magni. Porro quos quibusdam quisquam, voluptas sint culpa optio cumque ipsa corporis.
                </p>
                <p class="text-base dark:text-gray-400 mb-8">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Expedita, sed?
                </p>
                <span class="text-purple font-bold">PHP</span> and <span class="text-purple font-bold">JavaScript</span>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        var entries = [
            {label: 'HTML', url: 'https://www.w3schools.com/html/', target: '_blank'},
            {label: 'CSS', url: 'https://www.w3schools.com/css/', target: '_blank'},
            {label: 'JAVASCRIPT', url: 'https://www.w3schools.com/js/', target: '_blank'},
            {label: 'BOOTSTRAP', url: 'https://getbootstrap.com/', target: '_blank'},
            {label: 'TAILWIND CSS', url: 'https://tailwindcss.com/', target: '_blank'},
            {label: 'SASS', url: 'https://sass-lang.com/', target: '_blank'},
            {label: 'JQUERY', url: 'https://jquery.com/', target: '_blank'},
            {label: 'AJAX', url: 'https://www.w3schools.com/js/js_ajax_intro.asp', target: '_blank'},
            {label: 'ALPINE JS', url: 'https://alpinejs.dev/', target: '_blank'},
            {label: 'MYSQL', url: 'https://www.mysql.com/', target: '_blank'},
            {label: 'PHP', url: 'https://www.php.net/', target: '_blank'},
            {label: 'LARAVEL', url: 'https://laravel.com/', target: '_blank'},
            {label: 'FIGMA', url: 'https://www.figma.com/', target: '_blank'},
            {label: 'C', url: 'https://www.w3schools.com/c/', target: '_blank'},
            {label: 'C++', url: 'https://www.w3schools.com/cpp/', target: '_blank'},
            {label: 'WORDPRESS', url: 'https://wordpress.com/', target: '_blank'},
        ];

        var settings = {
            entries: entries,
            width: 800,
            height: 800,
            radius:' 65% ',
            radiusMin:75,
            bgDraw:true,
            bgColor:'#1F2024',
            opacityOver:1.00,
            opacityOut:0.05,
            opacitySpeed:6,
            fov:800,
            speed:2,
            fontSize:'30',
            fontColor:'#01ffe9',
            fontWeight:'bold',
            fontStyle:'normal',
            fontStrech:'normal',
            fontToUpperCase:true
        };

        $('#tag').svg3DTagCloud(settings);
    })
</script>
