    <!-- Navbar -->
    <header>
    <div
        class="navbar fixed flex justify-between items-center bg-white px-4 md:px-2 w-full py-2 border-b-[1px] border-white border-opacity-10 h-16 transition-all ease-in-out duration-500">

        <a href="#">
            <div
                class="logo flex text-lg md:text-xl items-center font-medium text-white lg:ml-[130px] transition-all ease-in-out duration-500">
                <img src="{{ asset('img/wedevelopstufflogo.svg') }}" alt="weDevelopStuff"
                    class="h-14 w-16 py-2 transition-all ease-in-out duration-500 text-dark dark:text-white" ">weDevelopStuff</div>
            </a>

            <nav>
                <button class=" md:hidden" @click="navbarOpen = !navbarOpen">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 lg:h-8 lg:w-8 text-white mt-1" fill="none"
                    viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
                </button>
                <ul class="fixed left-0 right-0 min-h-screen text-base mt-[5px] text-white space-y-5 md:mt-0 text-center transform translate-x-full transition-all duration-500 delay-200 ease-in-out md:relative md:flex md:min-h-0 md:space-y-0 md:space-x-6 md:p-0 md:translate-x-0 lg:mr-[100px] xl:mr-[150px]"
                    :class="{ 'translate-x-full': !navbarOpen }" :class="{ 'translate-x-0': navbarOpen }">
                    <li
                        class="hover:text-turquoise font-medium transition duration-300 delay-150 hover:delay-0 hover:ease-in-out border-t-[0.2px] border-b-[0.2px]  border-white border-opacity-10 md:border-none pt-4 md:pt-0 pb-5 md:pb-0">
                        <a href="#about" class="active" @click="navbarOpen = false">About</a></li>
                    <li
                        class="hover:text-turquoise font-medium transition duration-300 delay-150 hover:delay-0 hover:ease-in-out border-b-[0.2px] border-white border-opacity-10 md:border-none pb-5 md:pb-0">
                        <a href="#services" @click="navbarOpen = false">Services</a></li>
                    <li
                        class="hover:text-turquoise font-medium transition duration-300 delay-150 hover:delay-0 hover:ease-in-out border-b-[1px] border-white border-opacity-10 md:border-none pb-5 md:pb-0">
                        <a href="#portfolio" @click="navbarOpen = false">Portfolio</a></li>
                    <li
                        class="hover:text-turquoise font-medium transition duration-300 delay-150 hover:delay-0 hover:ease-in-out border-b-[1px] border-white border-opacity-10 md:border-none pb-5 md:pb-0">
                        <a href="#testimonials" @click="navbarOpen = false">Blog</a></li>
                    <li
                        class="hover:text-turquoise font-medium transition duration-300 delay-150 hover:delay-0 hover:ease-in-out border-b-[1px] border-white border-opacity-10 md:border-none pb-5 md:pb-0">
                        <a href="#contact" @click="navbarOpen = false">Contact</a></li>
                </ul>
                </nav>
            </div>
        </header>
            <!-- Navbar Section End -->
