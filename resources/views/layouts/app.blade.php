<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="dark">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WeDevelopStuff</title>
        <link rel="icon" type="image/x-icon" href="./img/wedevelopstufflogo.svg">
        <!-- Fonts -->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Orbitron:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">
        <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        @yield('head')

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body x-data="{ navbarOpen: false, scrolledFromTop: false }" x-init="window.pageYOffset >= 50 ? scrolledFromTop = true : scrolledFromTop = false"
        :class="{
            'overflow-hidden': navbarOpen,
            'overflow-auto': !navbarOpen
        }" class="antialiased relative z-0">
        <div data-scroll class="bg-gray-100 dark-bg-dark-gray">


  {{ $slot}}


        </div>
        @yield('scripts')
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/scroll.js') }}" defer></script>
        @yield('main')
        <script src="{{ asset('js/vanilla-tilt.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
        <script src="{{ asset('js/gsap.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.4/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.4/ScrollTrigger.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/jquery.svg3dtagcloud.js') }}"></script>


    </body>
</html>
