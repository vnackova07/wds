<x-app-layout>
    @section('main')
    <script src="{{ asset('js/main.js') }}"></script>
    @endsection
    <x-layout.navbar></x-layout.navbar>
    <x-home.hero></x-home.hero>
    <x-home.about></x-home.about>
    <x-home.services></x-home.services>
    <x-home.portfolio :projects="$projects"></x-home.portfolio>

    <x-layout.footer></x-layout.footer>

</x-app-layout>
