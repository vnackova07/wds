const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  darkMode: 'class',
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'dark-gray': '#1F2024',
                'purple': '#682ae9',
                'blue' : '#799ad7',
                'sky-blue': '#78c7e0',
                'turquoise': '#01ffe9',
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'), require('tailgrids/plugin')],
};
